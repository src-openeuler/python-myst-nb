%global _empty_manifest_terminate_build 0
%global pypi_name myst-nb

Name:           python-%{pypi_name}
Version:        1.1.2
Release:        1
Summary:        A collection of tools for working with Jupyter Notebooks in Sphinx.

License:        MIT
URL:            https://github.com/executablebooks/myst-nb
Source0:        %{url}/archive/v%{version}/myst_nb-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-virtualenv
BuildRequires:  python3-poetry-core
BuildRequires:  python3-flit-core

%description
A collection of tools for working with Jupyter Notebooks in Sphinx.


%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name}
A collection of tools for working with Jupyter Notebooks in Sphinx.

%prep
%autosetup -p1 -n MyST-NB-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitelib}/myst_nb/*
%{python3_sitelib}/myst_nb-*.dist-info/
%{_bindir}/mystnb*

%changelog
* Mon Oct 28 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 1.1.2-1
- Update package to version 1.1.2
  Fix incorrect output from prints originating from different processes

* Fri Aug 23 2024 xuhe <xuhe@kylinos.cn> - 1.1.1-1
- Update version to 1.1.1
- DOCS: set printoptions to disable modern scalar printing.
- FIX: output metadata overwrites image size for all following images.
- MAINT: fix dependency on pre-commit job.
- MAINT: appease mypy.
- MAINT: fix specs for CI matrix.
- DOCS: fix extra comma forgotten.
- FIX: remove incorrect license classifier.
- FIX: image metadata.
- MAINT: use findall instead of traverse.
- MAINT: restore default line length.
- DOCS: update changelog.

* Wed Nov 22 2023 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 1.0.0-1
- Upgrade package to version 1.0.0

* Fri Jul 28 2023 zoujiancang<jczou@isoftstone.com> - 0.17.2-1
- Initial package

